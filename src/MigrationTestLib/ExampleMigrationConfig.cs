﻿using System.Data;
using NukedBit.OrmLiteMigrations;
using ServiceStack.Data;
using ServiceStack.OrmLite;

namespace MigrationTestLib;

public class Person
{
    public string FirstName { get; set; }
    public string LastName { get; set; }
}


public class Address
{
    public string City { get; set; }
    public string Region { get; set; }
}

public class DummyMigration : Migration
{
    public override string MigrationId => "654170F3-B378-4C73-8E1A-95C0966A64F7";
    
    protected override async Task PerformMigrationAsync()
    {
        Db.CreateTableIfNotExists<Person>();
    }
}

public class DummyMigration2 : Migration
{
    public override string MigrationId => "21278001-419A-4A5A-B16B-B64887CC87B7";
    
    protected override async Task PerformMigrationAsync()
    {
        Db.CreateTableIfNotExists<Address>();
    }
}

public class ExampleMigrationConfig : MigrationConfig
{
    protected override void OnConfigure(IMigrationBuilder builder)
    {
        builder.AddMigration(new DummyMigration());
        builder.AddMigration(new DummyMigration2());
    }
}

public class ExampleConnectionFactory : MigrationConnectionFactory
{
    public override IDbConnection Create()
    {
        return (new OrmLiteConnectionFactory($"DataSource=test.db;Cache=Shared", SqliteDialect.Instance)).CreateDbConnection();
    }
}