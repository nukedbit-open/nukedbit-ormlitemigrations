using System.Reflection;
using System.Runtime.Loader;
using NukedBit.OrmLiteMigrations.Cli.Extensions;
using ServiceStack.OrmLite;
using Spectre.Console;

namespace NukedBit.OrmLiteMigrations.Cli;

public class MigrationApp : ConsoleAppBase
{
    private readonly IProcessExecutor _processExecutor;

    public MigrationApp(IProcessExecutor processExecutor)
    {
        _processExecutor = processExecutor ?? throw new ArgumentNullException(nameof(processExecutor));
    }

    [Command("seed")]
    public async Task SeedAsync([Option("n")]string name, [Option("c", "Debug or Release", DefaultValue = "Release")] string config = "Release")
    {
        try
        {
            await AnsiConsole.Progress()
                .Columns(new ProgressColumn[]
                {
                    new TaskDescriptionColumn(), // Task description
                    new SpinnerColumn(), // Spinner
                })
                .StartAsync(async ctx =>
                {
                    var migrationBuildTask = ctx.AddTask("Building project", new ProgressTaskSettings());
                    migrationBuildTask.IsIndeterminate = true;
                    migrationBuildTask.StartTask();
                    var seedingTask = ctx.AddTask("Seeding data", new ProgressTaskSettings());
                    seedingTask.IsIndeterminate = true;
                    
                    var assembly = await LoadMigrationAssembly(config);
                    var seedersTypes = FindSeedersTypes(assembly);
                    
                    migrationBuildTask.Increment(1);
                    migrationBuildTask.StopTask();
                    seedingTask.StartTask();
                    
                    if (!seedersTypes.Any())
                    {
                        throw new Exception("No seeders Found");
                    }

                    var connectionFactory = GetMigrationConnectionFactory(assembly);

                    var seeders = seedersTypes.Select(CreateSeederFrom).ToList();
                    var seeder = seeders.FirstOrDefault(s =>
                        s.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase));
                    if (seeder is null)
                    {
                        throw new Exception($"No seeder with {name} Found");
                    }
                    seedingTask.Description = $"Seeding database with seeder {name}";
                    await seeder.SeedAsync(connectionFactory.Create());
                });
            AnsiConsole.Reset();
            AnsiConsole.MarkupLine("[green]Migration completed[/] :party_popper:");
        }
        catch (Exception ex)
        {
           AnsiConsole.WriteException(ex,
               ExceptionFormats.ShortenPaths | ExceptionFormats.ShortenTypes |
               ExceptionFormats.ShortenMethods | ExceptionFormats.ShowLinks);
        }
    }

    private async Task<Assembly> LoadMigrationAssembly(string config)
    {

        var projectFileInfo = GetCurrentProjectFile();
        var projectFileName = projectFileInfo.Name;

        var exitCode = await _processExecutor.ExecuteAsync(new ProcessExecutorArgs("dotnet")
            .WithArgs("clean"));

        exitCode.ThrowIfInvalidExitCode();

        exitCode = await _processExecutor.ExecuteAsync(new ProcessExecutorArgs("dotnet")
            .WithArgs("build", "--configuration", config, "/p:CopyLocalLockFileAssemblies=true"));

        exitCode.ThrowIfInvalidExitCode();

        var dllFiles =
            projectFileInfo.Directory.GetFiles($"{Path.GetFileNameWithoutExtension(projectFileInfo.Name)}.dll",
                SearchOption.AllDirectories);

        var dllFile = dllFiles.First(path =>
            path.FullName.Contains($"{Path.DirectorySeparatorChar}bin{Path.DirectorySeparatorChar}{config}"));

        AssemblyLoadContext loadContext = AssemblyLoadContext.Default;
        loadContext.Resolving += (context, name) =>
        {
            var path = Path.Combine(dllFile.Directory.FullName, $"{name.Name}.dll");
            var assembly = Assembly.LoadFile(path);

            return assembly;
        };

        return loadContext.LoadFromAssemblyPath(dllFile.FullName);
    }


    [Command("migrate")]
    public async Task MigrateAsync([Option("c", "Debug or Release", DefaultValue = "Release")] string config = "Release")
    {
        try
        {
            await AnsiConsole.Progress()
                .Columns(new ProgressColumn[] 
                {
                    new TaskDescriptionColumn(),    // Task description
                    new SpinnerColumn(),            // Spinner
                })
                .StartAsync(async ctx =>
                {
                    var migrationBuildTask = ctx.AddTask("Building project", new ProgressTaskSettings());
                    migrationBuildTask.IsIndeterminate = true;
                    migrationBuildTask.StartTask();
                    var applyMigrationsTask = ctx.AddTask("Applying migrations", new ProgressTaskSettings());
                    applyMigrationsTask.IsIndeterminate = true;

                    var assembly = await LoadMigrationAssembly(config);
                    var migrationConfigsTypes = FindMigrationConfigsTypes(assembly);
                    migrationBuildTask.Increment(1);
                    migrationBuildTask.StopTask();
                    applyMigrationsTask.StartTask();
                    
                    if (migrationConfigsTypes.Count > 1)
                    {
                        throw new Exception("Only One Migration Config is allowed");
                    }
                    else if (!migrationConfigsTypes.Any())
                    {
                        throw new Exception("Not migrations Found");
                    }

                    var connectionFactory = GetMigrationConnectionFactory(assembly);

                    var migrationConfig = CreateFrom(migrationConfigsTypes.First());

                    var builder = Migration.NewBuilder();
                    
                    migrationConfig.Configure(builder);

                    var migrationExecutor = (builder as IMigrationExecutorBuilder).Build();

                    using var connection = connectionFactory.Create();
                    connection.Open(); 
                    await migrationExecutor.MigrateAsync(connection); 
                    applyMigrationsTask.StopTask();
                    
                });
            AnsiConsole.Reset();
            AnsiConsole.MarkupLine("[green]Migration completed[/] :party_popper:");
        }
        catch (Exception ex)
        {
            AnsiConsole.WriteException(ex, 
                ExceptionFormats.ShortenPaths | ExceptionFormats.ShortenTypes |
                ExceptionFormats.ShortenMethods | ExceptionFormats.ShowLinks);
        }
    }

    private MigrationConnectionFactory GetMigrationConnectionFactory(Assembly assembly)
    {
        var migrationConnectionFactoryTypes = FindMigrationConnectionFactoryTypes(assembly);

        if (migrationConnectionFactoryTypes.Count > 1)
        {
            throw new Exception("Only One Connection Factory is allowed");
        }
        else if (!migrationConnectionFactoryTypes.Any())
        {
            throw new Exception("Not connection Factory Found");
        }


        var connectionFactory = CreateFactoryFrom(migrationConnectionFactoryTypes.First());
        return connectionFactory;
    }


    private DatabaseSeeder CreateSeederFrom(Type type) => (DatabaseSeeder)Activator.CreateInstance(type);
    private MigrationConfig CreateFrom(Type type) => (MigrationConfig)Activator.CreateInstance(type);
    private MigrationConnectionFactory CreateFactoryFrom(Type type) => (MigrationConnectionFactory)Activator.CreateInstance(type);
    

    
    private List<Type> FindSeedersTypes(Assembly assembly)
    {
        return assembly.GetExportedTypes()
            .Where(t => t.IsSubclassOf(typeof(DatabaseSeeder)))
            .ToList();
    }
    
    private List<Type> FindMigrationConfigsTypes(Assembly assembly)
    {
        return assembly.GetExportedTypes()
            .Where(t => t.IsSubclassOf(typeof(MigrationConfig)))
            .ToList();
    }
    
    private List<Type> FindMigrationConnectionFactoryTypes(Assembly assembly)
    {
        return assembly.GetExportedTypes()
            .Where(t => t.IsSubclassOf(typeof(MigrationConnectionFactory)))
            .ToList();
    }


    private FileInfo GetCurrentProjectFile()
    {
        var currentFolder = Directory.GetCurrentDirectory();

        var projFiles = Directory.GetFiles(currentFolder, "*.csproj");

        if (projFiles.IsEmpty())
        {
            throw new Exception("No projects files found on current directory");
        }
        
        if (projFiles.Length > 1)
        {
            throw new Exception("Currently we only support one project file per directory");
        }

        return new FileInfo(projFiles.First());
    }
}