using System.Globalization;

namespace NukedBit.OrmLiteMigrations.Cli.Extensions;

public static class StringExtensions
{
    public static bool IsNullOrEmpty(this string s)
    {
        return string.IsNullOrEmpty(s);
    }

    public static bool IsNullOrEmptyOrWhiteSpace(this string s)
    {
        return string.IsNullOrEmpty(s) || string.IsNullOrWhiteSpace(s);
    }

    public static decimal ToDecimal(this string s, decimal defaultValue = 0)
    {
        s = s.Replace(",", ".");
        try
        {
            var n = decimal.Parse(s, CultureInfo.InvariantCulture);
            return n;
        }
        catch
        {
            return defaultValue;
        }
    }
    
    public static bool IsDecimal(this string s)
    {
        s = s.Replace(",", ".");
        
        return decimal.TryParse(s, NumberStyles.Float, CultureInfo.InvariantCulture, out var dec);
    }

    public static bool ContainsAnyOf(this string s, params char[] chars)
    {
        return chars.Any(s.Contains);
    }

    public static string Join(this IEnumerable<object> s, string separator)
    {
        return string.Join(separator, s);
    }
}