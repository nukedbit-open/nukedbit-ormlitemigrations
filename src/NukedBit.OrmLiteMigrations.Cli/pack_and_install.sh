#!/usr/bin/env bash
rm nupkg/NukedBit.OrmLiteMigrations.Cli.1.0.0.nupkg
dotnet tool uninstall  -g NukedBit.OrmLiteMigrations.Cli
dotnet pack
dotnet tool install -g NukedBit.OrmLiteMigrations.Cli --add-source ./nupkg