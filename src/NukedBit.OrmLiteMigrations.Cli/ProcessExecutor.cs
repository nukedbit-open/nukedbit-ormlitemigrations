using System.Diagnostics;
using NukedBit.OrmLiteMigrations.Cli.Extensions;

namespace NukedBit.OrmLiteMigrations.Cli;

public record ProcessExecutorArgs(string Executable)
{
    public string[] Arguments { get; init; } = Array.Empty<string>();
    public bool RedirectOutput { get; init; } = true;

    public string WorkingDirectory { get; init; } = Directory.GetCurrentDirectory();
    
    public ProcessExecutorArgs WithArgs(params string[] arguments) => this with { Arguments = arguments };
}

public interface IProcessExecutor
{
    Task<int> ExecuteAsync(ProcessExecutorArgs command, CancellationToken cancellationToken = default);
}

public class ProcessExecutor : IProcessExecutor
{
    public async Task<int> ExecuteAsync(ProcessExecutorArgs command, CancellationToken cancellationToken = default)
    {
        var startInfo = new ProcessStartInfo()
        {
            FileName = command.Executable,
            Arguments = command.Arguments.Join(" "),
            RedirectStandardOutput = command.RedirectOutput,
            RedirectStandardError = true
        };

        if (command.WorkingDirectory is not null)
        {
            startInfo.WorkingDirectory = command.WorkingDirectory;
        }
        
        var process = Process.Start(startInfo);
        
        if (command.RedirectOutput)
        {
             
            while(await process.StandardOutput.ReadLineAsync() is { } line)
            {
                // TODO: output to logger?!
               // Console.WriteLine(line);
            }
        }
        
        await process.WaitForExitAsync(cancellationToken);
        return process.ExitCode;
    }
}