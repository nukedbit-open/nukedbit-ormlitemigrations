namespace NukedBit.OrmLiteMigrations.Cli;

public static class ConsoleExtensions
{
    public static void ThrowIfInvalidExitCode(this int exitCode)
    {
        if (exitCode != 0)
        {
            throw new Exception("Failed migration executions");
        }
    }
}