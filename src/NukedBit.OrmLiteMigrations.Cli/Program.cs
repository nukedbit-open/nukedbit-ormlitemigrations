﻿
using Microsoft.Extensions.DependencyInjection;
using NukedBit.OrmLiteMigrations.Cli;

var builder = ConsoleApp.CreateBuilder(args);
builder.ConfigureServices((ctx,services) =>
{
    services.AddSingleton<IProcessExecutor, ProcessExecutor>();
    services.AddLogging(logging =>
    {
        logging.AddSimpleConsole();
    });
});

var app = builder.Build();

app.AddCommands<MigrationApp>();

await app.RunAsync();