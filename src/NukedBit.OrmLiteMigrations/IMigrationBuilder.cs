namespace NukedBit.OrmLiteMigrations;

public interface IMigrationBuilder: IMigrationExecutorBuilder
{
    /// <summary>
    /// Add your migration to the list of migrations to be applied to your db, migrations are applied, in FIFO
    /// </summary>
    /// <param name="migration"></param>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    IMigrationBuilder AddMigration<T>(T migration) where T : Migration;
}