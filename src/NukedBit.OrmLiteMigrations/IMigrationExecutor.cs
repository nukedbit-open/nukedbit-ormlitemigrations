using System.Data;

namespace NukedBit.OrmLiteMigrations;

public interface IMigrationExecutor
{
    Task MigrateAsync(IDbConnection db, CancellationToken cancellationToken = default);
}