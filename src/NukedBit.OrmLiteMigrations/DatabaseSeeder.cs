using System.Data;

namespace NukedBit.OrmLiteMigrations;

public abstract class DatabaseSeeder
{
    public abstract string Name { get; }
    
    protected IDbConnection Db { get; private set; }

    internal async Task SeedAsync(IDbConnection db)
    {
        Db = db;
        await OnSeedAsync();
    }

    protected abstract Task OnSeedAsync();
}