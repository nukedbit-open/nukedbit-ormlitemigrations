namespace NukedBit.OrmLiteMigrations;

public static class MigrationBuilderExtensions
{
    public static IMigrationBuilder ConfigureWith(this IMigrationBuilder builder, MigrationConfig config)
    {
        config.Configure(builder);
        return builder;
    }
}