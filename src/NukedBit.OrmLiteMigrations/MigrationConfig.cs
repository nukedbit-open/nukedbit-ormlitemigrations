namespace NukedBit.OrmLiteMigrations;

public abstract class MigrationConfig
{
    internal void Configure(IMigrationBuilder builder) => OnConfigure(builder);
    
    protected abstract void OnConfigure(IMigrationBuilder builder);
}