using System.Data;

namespace NukedBit.OrmLiteMigrations;

public abstract class Migration
{
    internal void SetIDbConnection(IDbConnection db) => Db = db;
    protected IDbConnection Db { get;  private set; }
    
    /// <summary>
    /// Specify a migration id, this id must be unique between migrations, and should not be dynamic
    /// as this will be used to check if migration is already applied
    /// </summary>
    public abstract string MigrationId { get; }

    internal virtual Task PerformMigrationInternalAsync() => PerformMigrationAsync();
    
    /// <summary>
    /// Implement here your own migration logic
    /// </summary>
    /// <returns></returns>
    protected abstract Task PerformMigrationAsync();
    
    public static IMigrationBuilder NewBuilder() => new MigrationBuilder();
}