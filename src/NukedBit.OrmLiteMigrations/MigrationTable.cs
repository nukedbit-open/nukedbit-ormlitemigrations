﻿using System.Runtime.CompilerServices;
using ServiceStack.DataAnnotations;

[assembly:InternalsVisibleTo("NukedBit.OrmLiteMigrations.Tests")]
[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")] 
[assembly: InternalsVisibleTo("NukedBit.OrmLiteMigrations.Cli")] 
namespace NukedBit.OrmLiteMigrations;

[Alias("_normlite_migration")]
public class MigrationTable
{
    [AutoIncrement]
    public int Id { get; set; }

    public DateTime CreatedOn { get; set; }
    
    [Unique]
    public string MigrationId { get; set; }
}