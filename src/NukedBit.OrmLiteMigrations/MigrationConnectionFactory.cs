using System.Data;

namespace NukedBit.OrmLiteMigrations;

public abstract class MigrationConnectionFactory
{
    public abstract IDbConnection Create();
}