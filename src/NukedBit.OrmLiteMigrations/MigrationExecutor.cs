using System.Data;
using ServiceStack;
using ServiceStack.OrmLite;

namespace NukedBit.OrmLiteMigrations;

internal class MigrationExecutor : IMigrationExecutor
{
    private readonly List<Migration> _migrations;

    public MigrationExecutor(List<Migration> migrations)
    {
        _migrations = migrations;
    }

    public async Task MigrateAsync(IDbConnection db, CancellationToken cancellationToken = default)
    {
        if (_migrations.Any(m => m.MigrationId.IsNullOrEmpty()))
        {
            throw new Exception("Migrations should specify a migration id");
        }
        using var trans = db.OpenTransaction();
        try
        {
            db.CreateTableIfNotExists<MigrationTable>();

            var existingMigrations = Enumerable.ToHashSet((await db.SelectAsync<MigrationTable>(token: cancellationToken))
                .Select(t => t.MigrationId));
            
            foreach (var migration in _migrations)
            {
                if(existingMigrations.Contains(migration.MigrationId))
                    continue;
                
                migration.SetIDbConnection(db);
                
                Console.WriteLine("Applying migration {0}", migration.MigrationId);
                await migration.PerformMigrationInternalAsync();
                await db.SaveAsync(new MigrationTable
                {
                    CreatedOn = DateTime.UtcNow,
                    MigrationId = migration.MigrationId
                }, token: cancellationToken);
            }
            
            trans.Commit();
        }
        catch (Exception)
        {
            trans.Rollback();
            throw;
        }
    }
}