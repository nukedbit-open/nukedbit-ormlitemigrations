namespace NukedBit.OrmLiteMigrations;

internal class MigrationBuilder : IMigrationBuilder, IMigrationExecutorBuilder
{
    private readonly List<Migration> _migrations = new();
    
    
    public IMigrationBuilder AddMigration<T>(T migration) where T: Migration
    {
        _migrations.Add(migration);
        return this;
    }

    public IMigrationExecutor Build()
    {
        return new MigrationExecutor(_migrations);
    }
}