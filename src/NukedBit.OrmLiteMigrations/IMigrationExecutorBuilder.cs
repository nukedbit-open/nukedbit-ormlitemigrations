namespace NukedBit.OrmLiteMigrations;

public interface IMigrationExecutorBuilder
{
    /// <summary>
    /// Create a migration executor
    /// </summary>
    /// <returns></returns>
    IMigrationExecutor Build();
}