# NukedBit OrmLiteMigrations

## Getting started

To install OrmLite Migrations Cli use

```
dotnet tool install -g NukedBit.OrmLiteMigrations.Cli --add-source https://gitlab.com/api/v4/projects/37773296/packages/nuget/index.json
```

To uninstall

```
dotnet tool uninstall -g NukedBit.OrmLiteMigrations.Cli
```

To reference NukedBit.OrmLiteMigrations inside your projects add the following feed to your nuget.config

https://gitlab.com/api/v4/projects/37773296/packages/nuget/index.json
