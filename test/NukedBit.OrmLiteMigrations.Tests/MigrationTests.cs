using System.Data;
using FluentAssertions;
using Moq;
using ServiceStack.OrmLite;
using ServiceStack.OrmLite.Sqlite;

namespace NukedBit.OrmLiteMigrations.Tests;

public class MigrationTests
{
    private IDbConnection CreateInMemoryDbConnection()
    {
        var factory = new OrmLiteConnectionFactory(":memory:", SqliteOrmLiteDialectProvider.Instance);
        return factory.OpenDbConnection();
    }

    [Test]
    public async Task ApplyMigrations()
    {
        var db = CreateInMemoryDbConnection();

        var migration1 = new Mock<Migration>();
        migration1.Setup(s => s.MigrationId)
            .Returns("migration1");
        var migration2 = new Mock<Migration>();
        migration2.Setup(s => s.MigrationId)
            .Returns("migration2");

        var builder = (Migration.NewBuilder()
            .AddMigration(migration1.Object)
            .AddMigration(migration2.Object) as IMigrationExecutorBuilder)
            .Build();
        
        await builder.MigrateAsync(db);

        var appliedMigrations = (await db.SelectAsync<MigrationTable>())
            .OrderByDescending(m => m.CreatedOn)
            .ToList();

        appliedMigrations
            .ElementAt(0)
            .MigrationId
            .Should()
            .Be("migration2");
        
        appliedMigrations
            .ElementAt(1)
            .MigrationId
            .Should()
            .Be("migration1");
        
        migration1.Verify(m => m.PerformMigrationInternalAsync(), Times.Once);
        migration2.Verify(m => m.PerformMigrationInternalAsync(), Times.Once);
    }
    
    [Test]
    public async Task Migration_Are_Applied_Only_Once()
    {
        var db = CreateInMemoryDbConnection();

        var migration1 = new Mock<Migration>();
        migration1.Setup(s => s.MigrationId)
            .Returns("migration1");
        var migration2 = new Mock<Migration>();
        migration2.Setup(s => s.MigrationId)
            .Returns("migration2");

        var builder = (Migration.NewBuilder()
            .AddMigration(migration1.Object)
            .AddMigration(migration2.Object) as IMigrationExecutorBuilder)
            .Build();
        
        await builder.MigrateAsync(db); // first round
        await builder.MigrateAsync(db); // second round should do nothing
        

        var appliedMigrations = (await db.SelectAsync<MigrationTable>())
            .OrderByDescending(m => m.CreatedOn)
            .ToList();

        appliedMigrations
            .ElementAt(0)
            .MigrationId
            .Should()
            .Be("migration2");
        
        appliedMigrations
            .ElementAt(1)
            .MigrationId
            .Should()
            .Be("migration1");
        
        migration1.Verify(m => m.PerformMigrationInternalAsync(), Times.Once);
        migration2.Verify(m => m.PerformMigrationInternalAsync(), Times.Once);
    }
    
    [Test]
    public async Task First_Migration_Is_Applied_Only_Once()
    {
        var db = CreateInMemoryDbConnection();

        var migration1 = new Mock<Migration>();
        migration1.Setup(s => s.MigrationId)
            .Returns("migration1");
        var migration2 = new Mock<Migration>();
        migration2.Setup(s => s.MigrationId)
            .Returns("migration2");

        var builder1 = (Migration.NewBuilder()
            .AddMigration(migration1.Object) as IMigrationExecutorBuilder)
            .Build();
        
        await builder1.MigrateAsync(db);
        
        
        var appliedMigrations = (await db.SelectAsync<MigrationTable>())
            .OrderByDescending(m => m.CreatedOn)
            .ToList();

        appliedMigrations.Count
            .Should().Be(1);
        
        
        var builder2 = (Migration.NewBuilder()
            .AddMigration(migration2.Object) as IMigrationExecutorBuilder)
            .Build();
        
        await builder2.MigrateAsync(db); // second round should do nothing
        

        appliedMigrations = (await db.SelectAsync<MigrationTable>())
            .OrderByDescending(m => m.CreatedOn)
            .ToList();

        appliedMigrations
            .ElementAt(0)
            .MigrationId
            .Should()
            .Be("migration2");
        
        appliedMigrations
            .ElementAt(1)
            .MigrationId
            .Should()
            .Be("migration1");
        
        migration1.Verify(m => m.PerformMigrationInternalAsync(), Times.Once);
        migration2.Verify(m => m.PerformMigrationInternalAsync(), Times.Once);
    }
}